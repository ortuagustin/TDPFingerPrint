unit DigitalPersona.FingerPrint.Compare;

{ Copyright (c) 2015 Ortu Agustin, ortu.agustin@gmail.com
Copyright (c) 2015 Ortu Agustin, https://gitlab.com/u/ortuagustin
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of copyright holders nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. }

interface

uses
  DPFPEngXLib_TLB, DPFPShrXLib_TLB;

type
  IFingerComparer = interface
    ['{61296897-162B-4202-ABE1-746923C10859}']
    function CompareTo(const Template: string): Boolean;
  end;

  TFingerComparer = class(TInterfacedObject, IFingerComparer)
  strict private
    FTemplate: IDPFPTemplate;
    FVerifier: IDPFPVerification;
    FCapturedSample: IDispatch;
  private
    function Compare(const CapturedSample: IDispatch; const Template: DPFPTemplate): Boolean;
  public
    constructor Create(ACapturedSample: IDispatch);
    function CompareTo(const Template: string): Boolean;
  end;

implementation

uses
  System.SysUtils, DigitalPersona.FingerPrint.Hashes;

{ TFingerComparer }

function TFingerComparer.Compare(const CapturedSample: IDispatch; const Template: DPFPTemplate): Boolean;
begin
  Result := DPFPVerificationResult(FVerifier.Verify(FCapturedSample, Template)).Verified;
end;

function TFingerComparer.CompareTo(const Template: string): Boolean;
begin
  FTemplate.Deserialize(WideBytesOf(FingerPrintHasher.Decrypt(Template)));
  Result := Compare(FCapturedSample, FTemplate);
end;

constructor TFingerComparer.Create(ACapturedSample: IDispatch);
begin
  inherited Create;
  FTemplate := CoDPFPTemplate.Create;
  FVerifier := CoDPFPVerification.Create;
  FCapturedSample := ACapturedSample;
end;

end.
