== TDPFingerPrint ==

English: Set of components that interfaces with DigitalPersona One Touch for Windows SDK, that allows Delphi developers
to quickly include biometric recognition to their applications

Requirements are:
- DigitalPersona One Touch for Windows SDK Version 1.6.1 (August 2010)
- ActiveX/COM DigitalPersona Components
- DCPcrypt Cryptographic Component Library v2 by David Barton

Supported readers according SDK documentation are:
- DigitalPersona U.are.U 4000B 
- DigitalPersona U.are.U 4500

I've tested only the U.are.U 4000B reader

Special thanks to
- [KingOfDragons](http://www.clubdelphi.com/foros/member.php?u=30175) of [ClubDelphi](url=http://www.clubdelphi.com)
 This work is heavily based on one of his [posts](url=http://www.clubdelphi.com/foros/showthread.php?t=72461)
- [David Barton](http://www.cityinthesky.co.uk/) for his Cryptographic Component Library

Installation:

- Installing the component and its dependencys

1. Open DPFingerPrintComps.groupproj
2. Right-click on DPFingerPrintComps in the Project Manager
3. Click on Build All
4. Right-click on DPFingerPrint.bpl in the Project Manager
5. Click on Install. This will install the TDPFingerPrint components and its dependencys

Note: The components were built using Delphi XE7. They also work fine under XE8 and Delphi 2010
To make them work in older versions of Delphi (ex Delphi 2010), you may need to change some unit uses sections:
In this way, Vcl.XXX becomes XXX. Example: Vcl.Forms --> Forms

- Adding the component unit to your search path

1. On Delphi IDE, click on Tools -> Options
2. Navigate to Enviroment Options -> Delphi Options -> Library
3. Add the following paths to the search path:
- ..\TDPFingerPrint                                -- The TDPFingerPrint main folder
- ..\TDPFingerPrint\third-party\DPComponents       -- The DigitalPersona ActiveX/COM type library imports
- ..\TDPFingerPrint\third-party\dcpcrypt2          -- The Cryptographic Component Library main folder
- ..\TDPFingerPrint\third-party\dcpcrypt2\Ciphers  -- The Cryptographic Component Library ciphers folder 
- ..\TDPFingerPrint\third-party\dcpcrypt2\Hashes   -- The Cryptographic Component Library hashes folder

Please note that these components only work under the VCL framework. They don't work on FMX

Usage:

Basically, we got two main components:

1. TDPFingerPrintReader: Used to capture the reading of the Fingerprint

Events:
- OnCaptured: Fires when a fingerprint is captured by the reader; provides an interface, IFingerComparer, that can be used
 to compare the captured fingerprint. 
 The captured fingerprint sample must satisfy CaptureFeedbackGood, otherwise, the event is not fired 
- OnFingerTouch, OnFingerGone: They map to the native events of the DigitalPersona reader
- OnStartCapture, OnStopCapture: Fired when the reader Starts/Stops capturing fingerprints

Properties
- AutoStartCapturing: if True, the reader will start capturing fingerprint right after the component is created
- ReaderPriority: Enumeration. Indicates the priority of the capturing process. For example, a Priority of rpHigh will capture 
  fingerprints even if the application is on the background
  
Methods are very straightforward:
- StartCapture 
- StartCapture(Priority)
- StartCapture(RunInBackground): Basically, sets priority to rpHigh and calls StartCapture
- StopCapture 

2. TDPFingerPrintEnroll: Used to enroll a new Fingerprint

It consist on a TCustomForm that acts as a container for a TDPFPEnrollmentControl
The TCustomForm is displayed Modally (ShowModal) on the Execute methods of the TDPFingerPrintEnroll
The form has also two buttons: a mrOk button and a mrCancel button

TDPFPEnrollmentControl is an ActiveX control included in the DPComponents
It provides a user friendly graphical interface to assist in the process of enrolling a fingerprint
In this UI, the user can select which finger is he intending to enroll the fingerprint, and start a process of 
capturing the fingerprint. It requires to capture 4 (four) samples, and then it creates the fingerprint value

Already enrolled fingerprints will be displayed as a green finger, while non-enrolled will be empty (gray)
Clicking on a non-enrolled finger will show the enrollement window where the user will be prompt to capture his fingerprint 4 (four) times
Clicking on an enrolled finger will prompt the user if he wants to delete that finger print

Events:
- OnFingerEnroll: Fires when a fingerprint is enrolled (a.k.a the four samples were collected and verified to be OK)
 It provides the TFinger which was enrolled and the actual fingerprint value
- OnFingerPrintDeleted: Fires when the user deletes an enrolled finger print. 
  Provides a TFinger indicating which finger fingerprint was deleted
- AfterExecute: Fires after the Execute method (really?). It will provide an array of string containing the
  enrolled fingerprints, a TFingerSet indicating which fingers were enrolled and a Boolean indicating wether the user clicked
  on Ok button (Result = True) or Cancel button (Result = False)

Properties:
- EnrolledFingers: A set of TFinger containing the currently enrolled fingers
- MaxFingerPrintsAllowed: Integer. Indicates the max amount of fingers that can be enrolled

Methods:
- Execute(ClearFingerPrints): First, checks wether the reader is capturing. When enrolling, the capturing must
 be stopped. Otherwise it will not work. Then, modally shows the container form. The Boolean param indicates wether
 it must reset the enrolled fingerprints or no
- Execute(EnrolledFingers): Overlodad version of the prior. It will reset the enrolled fingers and set to as-enrolled 
 the ones in the TFingerSet provided in the EnrolledFingers param